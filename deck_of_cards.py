import random
import unittest

class Card(object):

    def __init__(self, suit, val):
        self.suit = suit
        self.value = val

    def showCard(self):
        if self.value == 1:
            print("A of {}".format(self.suit))
        elif self.value == 11:
            print("J of {}".format(self.suit))
        elif self.value == 12:
            print("Q of {}".format(self.suit))
        elif self.value == 13:
            print("K of {}".format(self.suit))
        else:
            print("{} of {}".format(self.value, self.suit))


class Deck(object):
    def __init__(self):
        self.cards = []
        self.build()

    def build(self):
        for s in ["Piques", "Carreaux", "Cœurs", "Trèfles"]:
            for v in range(1, 14):
                self.cards.append(Card(s, v))

    def showCards(self):
        for c in self.cards:
            c.showCard()

    def shuffle(self):
        for i in range(len(self.cards)-1, 0, -1):
            r = random.randint(0, i)
            self.cards[i], self.cards[r] = self.cards[r], self.cards[i]

    def drawCard(self):
        return self.cards.pop()

class Player(object):

    def __init__(self, name):
        self.name = name
        self.hand = []

    def drawHand(self, deck, num):
        for i in range(num):
            self.hand.append(deck.drawCard())

    def showHand(self):
        return self.hand

    def discard(self, index):
        self.hand.pop(index)

class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
